from django.shortcuts import render
from .models import Book
from django.contrib.auth.decorators import login_required


def index(request):
    books = Book.objects.all()
    context = {
        'books': books
    }
    return render(request, 'books/index.html', context=context)


@login_required
def detail(request, id):
    book = Book.objects.get(id=id)
    return render(request, 'books/detail.html', context={'book': book})
