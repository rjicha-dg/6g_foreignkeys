from django.db import models


class Author(models.Model):
    first_name = models.CharField(max_length=50, null=False, blank=False)
    last_name = models.CharField(max_length=50, null=False, blank=False)

    def __str__(self):
        return self.first_name + " " + self.last_name


class Book(models.Model):
    title = models.CharField(max_length=255, null=False, blank=False)
    description = models.TextField(null=False, blank=False)
    author = models.ForeignKey(Author, null=True, blank=True, on_delete=models.SET_NULL)
    cover = models.ImageField(upload_to='images', blank=True, null=True)

    def __str__(self):
        return self.title
